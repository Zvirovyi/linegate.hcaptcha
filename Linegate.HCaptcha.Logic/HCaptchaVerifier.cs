using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json;
using System.Threading.Tasks;

namespace Linegate.HCaptcha.Logic
{
    public class HCaptchaVerifier
    {
        private string secret;
        
        private HttpClient httpClient = new();

        public HCaptchaVerifier(string secret)
        {
            this.secret = secret;
        }

        public async Task<bool> Verify(string captchaResponse)
        {
            FormUrlEncodedContent captchaTest = new(new[]
            {
                new KeyValuePair<string?, string?>("response", captchaResponse),
                new KeyValuePair<string?, string?>("secret", secret)
            });
            var captchaTestResponse = await httpClient.PostAsync("https://hcaptcha.com/siteverify", captchaTest);
            var captchaTestJson = JsonDocument.Parse(await captchaTestResponse.Content.ReadAsStringAsync()).RootElement;
            return captchaTestJson.GetProperty("success").GetBoolean();
        }
    }
}