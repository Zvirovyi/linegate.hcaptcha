using Microsoft.Extensions.DependencyInjection;

namespace Linegate.HCaptcha.Component
{
    public static class HCaptchaServiceExtensions
    {
        public static IServiceCollection AddHCaptchaService(this IServiceCollection services, string sitekey)
        {
            services.AddSingleton(new HCaptchaService(sitekey));
            return services;
        }
    }
}