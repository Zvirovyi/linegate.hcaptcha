namespace Linegate.HCaptcha.Component
{
    public class HCaptchaService
    {
        private readonly string sitekey;

        public string? CaptchaID;
        
        public HCaptchaService(string sitekey)
        {
            this.sitekey = sitekey;
        }

        public string Sitekey => sitekey;
    }
}