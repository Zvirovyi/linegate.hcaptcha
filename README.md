# Linegate.HCaptcha

HCaptcha razor component for Blazor WASM and option to verify its result on server-side.

# How to use it on client

1. At first, include this project.
2. Add js for hCaptcha into index.html: `<script src="https://js.hcaptcha.com/1/api.js?render=explicit&recaptchacompat=off"></script>`.
3. Add captcha service: `builder.Services.AddHCaptchaService("<your-sitekey>");`.
4. Add required imports into `_Imports.razor`: `@using Linegate.HCaptcha.Component`.
5. Use it as component.

Example of component usage:

```c#
<HCaptcha @ref="captcha"/>

@code {
    private HCaptcha captcha;

    private async void OnButton()
    {
        Console.WriteLine(await captcha.GetResponse());
    }

}
```

## How to use it on server

1. Include this project.
2. Get captcha response from client.
3. Create HCaptchaVerifier: `HCaptchaVerifier verifier = new("<your-secret>")`.
4. Check result: `var result = await verifier.Verify("<captcha-response>")`.